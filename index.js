const express = require("express");
const app = express();
const PORT = process.env.PORT || 4400;

app.get("/", (req, res) => {
    const date = new Date();
    res.send(date)
})


app.get("/download", (req, res) => {
    const savingFilename = 'report.pdf'
    res.setHeader('Content-disposition', `attachment; filename=${savingFilename}`);
    const id = req.query.id;
    let buff = Buffer.from(id, 'base64');  
    let reportName = buff.toString('utf-8');

    const file = `${__dirname}/files/${reportName}.pdf`;
    res.download(file); 
})


app.listen(PORT, () => console.log(`App is running at port ${PORT}`))